/********************************************************************
* file TIM_Defines.h
* brief Bit Config Defines Lib
* author Sofiane AOUCI for TIM
* version 1.0
* date 01/03/2019 
********************************************************************/

#ifndef TIM_DEFINES_H
#define TIM_DEFINES_H

/*---------------------------------- Oscillator Selection bits ---------------------------------*/
#define TIM_INTOSC_FOSC
//#define TIM_INTOSC_FOSC_CLKOUT
//#define TIM_INTOSC_RC
//#define TIM_INTOSC_RC_CLKOUT
//#define TIM_INTOSC_LP
//#define TIM_INTOSC_XT
//#define TIM_INTOSC_EC_LOW
//#define TIM_INTOSC_EC_LOW_CLKOUT
//#define TIM_INTOSC_EC_MEDIUM
//#define TIM_INTOSC_EC_MEDIUM_CLKOUT
//#define TIM_INTOSC_EC_HIGH
//#define TIM_INTOSC_EC_HIGH_CLKOUT
//#define TIM_INTOSC_EC_MEDIUM
//#define TIM_INTOSC_EC_HIGH

/*-------------------------------------- PLL Selection bits -------------------------------------*/
//#define TIM_PLL_OFF

/*---------------------------- Secondary Oscillator Mode Selection bits --------------------------*/
//#define TIM_HIGH_SOSC
//#define TIM_LOW_SOSC

/*--------------------------------- MCLR/RE3Input Selection bits ---------------------------------*/
//#define TIM_RE3_INPUT

/*----------------------------------- CAN PORT Selection bits ------------------------------------*/
#define TIM_CAN_PORTC

#endif