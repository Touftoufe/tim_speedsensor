#include "TIM_GPIO.h"

TIM_GPIO_InitTypDef  new_GPIO_InitDef()
{
	TIM_GPIO_InitTypDef GPIO_InitDef;
	GPIO_InitDef.pull_up = false;
	return GPIO_InitDef;
}

void TIM_GPIO_Init(TIM_GPIO_InitTypDef* GPIO_InitDef)
{
	uint8_t* TRISx = (uint8_t*) (GPIO_InitDef->port + 0x12);
	uint8_t* ANCONx = (uint8_t*)((uint8_t)(GPIO_InitDef->port == GPIOA) * (uint8_t)&ANCON0 + (uint8_t)(GPIO_InitDef->port == GPIOB) * (uint8_t)&ANCON1);
	INTCON2bits.nRBPU = 0;

	if (GPIO_InitDef->mode == GPIO_Mode_OUT)
	{
		*TRISx  &= ~ (GPIO_InitDef->pin & 0xFF);
		//if(GPIO_InitDef->port != GPIOC) *ANCONx &= ~ (GPIO_InitDef->pin & 0xFF);
	}
	else
	{
		*TRISx |= GPIO_InitDef->pin & 0xFF;
		if (GPIO_InitDef->mode == GPIO_Mode_AN)	
            *ANCONx |= (GPIO_InitDef->pin >> 0x08) & 0xFF * (uint8_t)(GPIO_InitDef->port == GPIOA) + (GPIO_InitDef->pin >> 0x10) & 0xFF * (uint8_t)(GPIO_InitDef->port == GPIOB) ;
		else	
            *ANCONx &= ~((GPIO_InitDef->pin >> 0x08) & 0xFF) * (uint8_t)(GPIO_InitDef->port == GPIOA) + ~((GPIO_InitDef->pin >> 0x10)) & 0xFF * (uint8_t)(GPIO_InitDef->port == GPIOB) ;
	
    }

	if (GPIO_InitDef->pull_up && GPIO_InitDef->port == GPIOB)	WPUB |= GPIO_InitDef->pin & 0xFF;
	else if (!GPIO_InitDef->pull_up && GPIO_InitDef->port == GPIOB)	WPUB &= ~ (GPIO_InitDef->pin & 0xFF);
}

void TIM_GPIO_WriteBits(TIM_GPIO_Port GPIOx, TIM_GPIO_Pin pins, TIM_GPIO_State state)
{
	uint8_t* LATx = (uint8_t*) (GPIOx + 0x09);
	if(state)	*LATx |= pins & 0xFF;
	else *LATx &= ~ (pins & 0xFF);
}

TIM_GPIO_State TIM_GPIO_ReadBit(TIM_GPIO_Port GPIOx, TIM_GPIO_Pin pin)
{
	uint8_t* port = (uint8_t*)GPIOx;
	return *port & (pin & 0xFF);
}

void TIM_GPIO_ToggleBits(TIM_GPIO_Port GPIOx, TIM_GPIO_Pin pins)
{
	uint8_t* LATx = (uint8_t*)(GPIOx + 0x09);
	*LATx ^=  pins & 0xFF;
}
