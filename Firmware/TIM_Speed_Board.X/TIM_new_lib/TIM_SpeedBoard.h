/* 
 * File: TIM_SpeedBoard.h 
 * Author: Sofiane AOUCI
 * Comments: Speed board Project
 * Revision history: V 1.0
 */


#ifndef TIM_SPEEDBOARD_H
#define TIM_SPEEDBOARD_H

#include "TIM_Defines.h"
#include "TIM_ConfigurationBits.h"

#include <xc.h>
#include <stdint.h>
#include <stdbool.h>
#include "TIM_CLOCK.h"
#include "TIM_GPIO.h"
#include "TIM_EXTI.h"
#include "TIM_TIMER.h"
#include "ecan.h"
#include "TIM_SpeedBoard_Control.h"

/****************************** TARGET SYSTEM ******************************/

#ifdef TIM_TIM7_TEST_Banc

    #define WHEEL_CIRCUMFERNCE_MM 1500.0   // WHEEL CIRCUMFERENCE [mm] (pi*D)
    #define NB_MAGNETS 4                // NUMBER OF MAGNETS

#endif

#ifdef TIM_TIM8_TEST_Banc

    #define WHEEL_CIRCUMFERNCE_MM ?                                   
    #define NB_MAGNETS ?

#endif

#ifdef TIM_TIM7

    #define WHEEL_CIRCUMFERNCE_MM ?                                   
    #define NB_MAGNETS ?

#endif

#ifdef TIM_TIM8

    #define WHEEL_CIRCUMFERNCE_MM 1445.133
    #define NB_MAGNETS 4

#endif


#define exponent(a) (int)1e##a

#define TIM0_Freq 16000000.0  //Mhz
#define TIM0_CounterSize 255.0
#define TIM0_Prescaler 8.0
#define TIM_TIMEOUT 2 // seconds
#define Speed_Precision exponent(1) // the number of decimals


uint32_t volatile TIM0_Counter;
uint32_t volatile passage[2];
uint32_t volatile TIM_TimeOut;
uint32_t volatile intervals[NB_MAGNETS];
uint8_t volatile magnet;

void TIM_SpeedBoard_Init();
void TIM_SpeedBoard_SendSpeed(uint32_t speed);
uint32_t TIM_SpeedBoard_GetSpeed();
uint32_t (*TIM_Speed)(void);

#endif