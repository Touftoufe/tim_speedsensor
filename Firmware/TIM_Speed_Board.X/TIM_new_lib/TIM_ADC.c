#include "TIM_ADC.h"

TIM_ADC_InitTypDef  new_ADC_InitDef()
{
	TIM_ADC_InitTypDef ADC_InitDef;

	ADC_InitDef.nchannel	= ADC_nChannel_AVss;
	ADC_InitDef.vref		= ADC_Vref_AVdd;
	ADC_InitDef.nvref		= ADC_nVref_AVss;
	ADC_InitDef.trigsel		= ADC_TRIGSEL_ECCP1;
	ADC_InitDef.rf			= ADC_RF_Right;
	ADC_InitDef.clock		= ADC_CLOCK_FOSC_64;
	ADC_InitDef.acqt		= ADC_ACQT_20TAD;
	ADC_InitDef.enable		= false;
	return ADC_InitDef;
}

void TIM_ADC_Init(TIM_ADC_InitTypDef* ADC_InitDef)
{
	ADCON0bits.CHS = ADC_InitDef->channel;
	ADCON1bits.CHSN = ADC_InitDef->nchannel;
	ADCON1bits.VCFG = ADC_InitDef->vref;
	ADCON1bits.VNCFG = ADC_InitDef->nvref;
    ADCON1bits.TRIGSEL = ADC_InitDef->trigsel;
	ADCON2bits.ADFM = ADC_InitDef->rf;
	ADCON2bits.ACQT = ADC_InitDef->acqt;
    ADCON2bits.ADCS = ADC_InitDef->clock;
    ADCON0bits.ADON = ADC_InitDef->enable;
}

void TIM_ADC_enable(bool enable)
{
	while(ADCON0bits.nDONE);	
	ADCON0bits.ADON = enable;	
}

void TIM_ADC_newChannel(TIM_ADC_InitTypDef* ADC_InitDef)
{
	while(ADCON0bits.nDONE);
	ADCON0bits.CHS = ADC_InitDef->channel;
}

void TIM_ADC_StartConversion()
{
    ADCON0bits.GO=1;
}

bool TIM_ADC_IsBusy()
{
    if(ADCON0bits.nDONE)
        return true;
    else
        return false;
}

int16_t TIM_ADC_GetResult()
{   
	return (ADRESH << 8) + ADRESL;
	
}
int16_t TIM_ADC_GetConversion()
{
    TIM_ADC_StartConversion();
    while(TIM_ADC_IsBusy());
    return TIM_ADC_GetResult();
}
