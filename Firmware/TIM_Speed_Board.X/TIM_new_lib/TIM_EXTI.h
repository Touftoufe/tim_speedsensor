/* 
 * File:   TIM_EXTI.h
 * Author: beusaile
 *
 * Created on July 18, 2019, 7:28 PM
 */

#ifndef TIM_EXTI_H
#define	TIM_EXTI_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <xc.h>
#include <stdint.h>
#include <stdbool.h>
    
typedef union {
    struct {
        unsigned INT1_FLAG                  :1;
        unsigned INT2_FLAG                  :1;
        unsigned INT3_FLAG                  :1;
        unsigned INT1_ENABLE                :1;
        unsigned INT2_ENABLE                :1;
        unsigned INT3_ENABLE                :1;
        unsigned                            :2;
        
        unsigned                            :3;
        unsigned INT3_ON_RISING             :1;
        unsigned INT2_ON_RISING             :1;
        unsigned INT1_ON_RISING             :1;
        unsigned INT0_ON_RISING             :1;
        unsigned                            :1;
        
        unsigned                            :1;
        unsigned INT0_FLAG                  :1;
        unsigned                            :2;
        unsigned INT0_ENABLE                :1;
        unsigned                            :2;
        unsigned GLOBAL_INT_ENABLE          :1;
    };
}TIM_EXTI_t;

volatile TIM_EXTI_t *TIM_EXTI = (TIM_EXTI_t*) &INTCON3bits; 

#ifdef	__cplusplus
}
#endif

#endif	/* TIM_EXTI_H */

