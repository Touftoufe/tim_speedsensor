/********************************************************************
* file TIM_ConfigurationBits.h
* brief System Configuration Bits Lib
* author Sofiane AOUCI for TIM
* version 1.0
* date 01/03/2019 
********************************************************************/

#ifndef TIM_CONFIGURATION_BITS_H
#define TIM_CONFIGURATION_BITS_H

#include <p18cxxx.h>
#include "p18f26k80.h"


#pragma config XINST    = OFF        // Extended Instruction Set Disabled, otherwise the libs will not work

/*---------------------------------- Oscillator Selection bits ---------------------------------*/
#if defined TIM_INTOSC_FOSC
#pragma config FOSC	= INTIO2

#elif defined TIM_INTOSC_FOSC_CLKOUT
#pragma config FOSC	= INTIO1

#elif defined TIM_INTOSC_RC
#pragma config FOSC	= RC

#elif defined TIM_INTOSC_RC_CLKOUT
#pragma config FOSC	= RCIO

#elif defined TIM_INTOSC_LP
#pragma config FOSC	= LP

#elif defined TIM_INTOSC_XT
#pragma config FOSC	= XT

#elif defined TIM_INTOSC_EC_LOW
#pragma config FOSC	= EC1

#elif defined TIM_INTOSC_EC_LOW_CLKOUT
#pragma config FOSC	= EC1IO

#elif defined TIM_INTOSC_EC_MEDIUM
#pragma config FOSC	= EC2

#elif defined TIM_INTOSC_EC_MEDIUM_CLKOUT
#pragma config FOSC	= EC2IO

#elif defined TIM_INTOSC_EC_HIGH
#pragma config FOSC	= EC3

#elif defined TIM_INTOSC_EC_HIGH_CLKOUT
#pragma config FOSC	= EC3IO

#elif defined TIM_INTOSC_EC_MEDIUM
#pragma config FOSC	= HS1

#elif defined TIM_INTOSC_EC_HIGH
#pragma config FOSC	= HS2

#endif
/*-------------------------------------------------------------------------------------------------------------*/

/*--------------------------------------------- PLL Selection bits --------------------------------------------*/
#ifndef TIM_PLL_OFF
#pragma config PLLCFG   =   ON
#endif
/*-------------------------------------------------------------------------------------------------------------*/

/*---------------------------------- Secondary Oscillator Mode Selection bits ---------------------------------*/
#if defined TIM_HIGH_SOSC
#pragma config SOSCSEL = HIGH 

#elif defined TIM_LOW_SOSC
#pragma config SOSCSEL = LOW       

#else
#pragma config SOSCSEL = DIG

#endif
/*-------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------- MCLR/RE3Input Selection bits ---------------------------------------*/
#ifdef TIM_RE3_INPUT
#pragma config MCLRE = OFF       // Master Clear disabled
#endif
/*-------------------------------------------------------------------------------------------------------------*/

/*------------------------------------------ CAN PORT Selection bits ------------------------------------------*/
#ifdef TIM_CAN_PORTC
#pragma config CANMX = PORTC     // CAN on RC6/RC7
#endif
/*-------------------------------------------------------------------------------------------------------------*/

#endif
