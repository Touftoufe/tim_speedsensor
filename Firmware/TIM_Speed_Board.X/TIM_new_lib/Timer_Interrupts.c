/*
 * File:   Timer_Interrupts.c
 * Author: Sofiane AOUCI
 *
 * Created on March 7, 2019, 2:45 PM
 */

#include <pic18f26k80.h>
#include "Timer_Interrupts.h"

void Interr_Init()
{
    

    //TRISBbits.TRISB0 = 1 ;
    //TRISBbits.TRISB1 = 1 ;
    
    INTCONbits.INT0IF = 0 ; //External interrupt flag
    INTCONbits.INT0IE = 1 ; //External interrupts enable bit
    INTCONbits.TMR0IE = 1 ; //TIMER0 interrupt enable bit 
    INTCONbits.TMR0IF = 0; //clearing TIMER0 flag 
    INTCON2bits.INTEDG0 = 0 ; // INT0 interrupt on rising edge 1 / falling 0
    INTCONbits.GIE= 1 ; //Global interrupt enable 
    

}

void SpeedTimerInit()
{
    
    T0CONbits.T08BIT = 1 ; // 8 bit counter enabled
    T0CONbits.T0CS = 0 ; //clock source
    T0CONbits.PSA = 1 ; //prescaler desabled
    //T0CONbits.T0PS2 = 1 ;
    //T0CONbits.T0PS1 = 1 ;
    //T0CONbits.T0PS0 = 1 ;
    TMR0 = 0 ;
    //8 bit counter, no prescaler
    T0CONbits.TMR0ON = 1 ; //TIMER0 enabled
}

void interrupt tc_int(void)
{
	if (INTCONbits.TMR0IF)
    {  
            INTCONbits.TMR0IF = 0;				//clear interrupt flag
            ++TIM0_Counter;
    }
            
        if(INTCONbits.INT0IF == 1)
        {
            
        }
}