/********************************************************************
* file: 	TIM_ADC.h
* brief: 	ADC configuration Lib
* author: 	Sofiane AOUCI for TIM
* version: 	1.0
* date:		03/03/2019 
********************************************************************/

#ifndef TIM_ADC_H
#define TIM_ADC_H
#include <xc.h>
#include <stdint.h>
#include <stdbool.h>

typedef enum
{
	ADC_Channel_0   = 0,
	ADC_Channel_1   = 1,
	ADC_Channel_2   = 2,
	ADC_Channel_3   = 3,
	ADC_Channel_4   = 4,
	ADC_Channel_5   = 5,
	ADC_Channel_6   = 6,
	ADC_Channel_7   = 7,
	ADC_Channel_8   = 8,
	ADC_Channel_9   = 9,
	ADC_Channel_10  = 10,
	ADC_Channel_28  = 28,	//MUX
	ADC_Channel_29  = 29,	//Temperature diode
	ADC_Channel_30  = 30,	//VDDCORE
	ADC_Channel_31  = 31	//1.024V band gap
} TIM_ADC_Channel;

typedef enum
{
	ADC_nChannel_AVss  = 0,
	ADC_nChannel_AN0   = 1,
	ADC_nChannel_AN1   = 2,
	ADC_nChannel_AN2   = 3,
	ADC_nChannel_AN3   = 4,
	ADC_nChannel_AN4   = 5,
	ADC_nChannel_AN5   = 6,
} TIM_ADC_nChannel;


typedef enum
{
	ADC_Vref_4V 	  = 0b11,
	ADC_Vref_2V 	  = 0b10,
	ADC_Vref_External = 0b01,	
	ADC_Vref_AVdd	  = 0b00
}TIM_ADC_Vref;

typedef enum
{
	ADC_nVref_External = 0b01,	
	ADC_nVref_AVss	  = 0b00
}TIM_ADC_nVref;

typedef enum
{
	ADC_RF_Right 	  = 1,
	ADC_RF_Left 	  = 0
}TIM_ADC_Result_Format;

typedef enum
{
	ADC_ACQT_20TAD 	  = 0b111,
	ADC_ACQT_16TAD 	  = 0b110,
	ADC_ACQT_12TAD 	  = 0b101,
	ADC_ACQT_8TAD 	  = 0b100,
	ADC_ACQT_6TAD 	  = 0b011,
	ADC_ACQT_4TAD 	  = 0b010,
	ADC_ACQT_2TAD 	  = 0b001,
	ADC_ACQT_0TAD 	  = 0b000	
}TIM_ADC_ACQT;

typedef enum
{
	ADC_CLOCK_FRC 	  	  = 0b111,
	ADC_CLOCK_FOSC_64 	  = 0b110,
	ADC_CLOCK_FOSC_16 	  = 0b101,
	ADC_CLOCK_FOSC_4 	  = 0b100,
//	ADC_CLOCK_FRC 		  = 0b011,
	ADC_CLOCK_FOSC_32 	  = 0b010,
	ADC_CLOCK_FOSC_8 	  = 0b001,
	ADC_CLOCK_FOSC_2 	  = 0b000	
}TIM_ADC_CLOCK;

typedef enum
{
	ADC_TRIGSEL_CCP2 	  = 0b11,
	ADC_TRIGSEL_TIM1 	  = 0b10,
	ADC_TRIGSEL_CTMU 	  = 0b01,
	ADC_TRIGSEL_ECCP1 	  = 0b00	
}TIM_ADC_TRIGSEL;


typedef struct
{
	TIM_ADC_Channel channel;
	TIM_ADC_nChannel nchannel;
	TIM_ADC_Vref vref;
	TIM_ADC_nVref nvref;
	TIM_ADC_TRIGSEL trigsel;
	TIM_ADC_Result_Format rf;
	TIM_ADC_CLOCK clock;
	TIM_ADC_ACQT acqt;
	bool enable;
} TIM_ADC_InitTypDef;

TIM_ADC_InitTypDef  new_ADC_InitDef();
void TIM_ADC_Init(TIM_ADC_InitTypDef* ADC_InitDef);
void TIM_ADC_enable(bool enable);
void TIM_ADC_newChannel(TIM_ADC_InitTypDef* ADC_InitDef);
void TIM_ADC_StartConversion();
bool TIM_ADC_IsBusy();
int16_t TIM_ADC_GetResult();
int16_t TIM_ADC_GetConversion();
#endif