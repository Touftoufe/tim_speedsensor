/**
  ECAN Generated Driver API Header File

  @Company
    Microchip Technology Inc.

  @File Name
    ecan.h

  @Summary
    This is the generated header file for the ECAN driver using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  @Description
    This header file provides APIs driver for ECAN.
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.76
        Device            :  PIC18F26K80
        Driver Version    :  2.11
    The generated drivers are tested against the following:
        Compiler          :  XC8 2.00
        MPLAB 	          :  MPLAB X 5.10
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

#ifndef ECAN_H
#define ECAN_H


/**
  Section: Included Files
*/

#include <stdbool.h>
#include <stdint.h>
#include "TIM_Defines.h"

#ifdef __cplusplus  // Provide C++ Compatibility

    extern "C" {

#endif

/**

Global Defines  

*/
        
#define BROADCAST_ADDR			0xFF
#define NOT_SPECIFIED_ADDR			0x0
#define DASHBOARD_ADDR			0x1
#define STEARING_WHEEL_ADDR			0x2
#define GATEWAY_ADDR			0x3
#define THERMAL_ENGINE_ADDR			0xFD
#define ELECTRICAL_MOTOR_ADDR			0xFE
#define BACK_LIGHT_ADDR			0x31
#define FRONT_LIGHT_ADDR			0x32
#define GLOBAL_LIGHT_ADDR			0x30
#define BLUETOOTH_ADDR			0xE1
#define XBEE_SD_ADDR			0xE0
#define POSITION_MONITORING_ADDR			0x40
#define SIMULATION_ADDR			0xD0
#define UTRA_CAPACITOR_PROTECTION_ADDR			0xFC


#define AT07_THERMAL_ENABLE_INJECT_CMD			0x400
#define AT07_THERMAL_STARTER_CMD			0x402
#define AT07_THERMAL_CARTO_CFG			0x200
#define AT07_THERMAL_BOARD_STATUS			0x600
#define AT07_RUN_INFO_DATA			0x820
#define AT07_LIGHTS_CMD			0x420
#define AT07_LIGHTS_RESP			0x421
#define AT07_BLINKERS_CMD			0x422
#define AT07_BLINKERS_RESP			0x423
#define AT07_FL_LIGHT_CFG			0x222
#define AT07_FL_LIGHT_RESP			0x223
#define AT07_FR_LIGHT_CFG			0x220
#define AT07_FR_LIGHT_RESP			0x221
#define AT07_BL_LIGHT_CFG			0x224
#define AT07_BL_LIGHT_RESP			0x225
#define AT07_BR_LIGHT_CFG			0x226
#define AT07_BR_LIGHT_RESP			0x227
#define AT07_BLINK_TIME_CFG			0x228
#define AT07_BLINK_TIME_RESP			0x229
#define AT07_LIGHTS_STATUS_REQ			0x620
#define AT07_MSG_PILOT1_DATA			0x860
#define AT07_MSG_PILOT2_DATA			0x862
#define AT07_MSG_PILOT3_DATA			0x864
#define AT07_SPEED_DATA                 0x806
#define AT07_BATTERY_DATA               0x840
#define AT07_POSITION_LAT_DATA			0x800
#define AT07_POSITION_LONG_DATA			0x802
#define AT07_POSITION_MODE_DATA			0x804
#define AT07_ELEC_MOTOR_FORWARD_CMD			0x460
#define AT07_ELEC_MOTOR_BACKWARD_CMD			0x462


#define AT07_THERMAL_ENABLE_INJECT_LENGTH			1
#define AT07_THERMAL_STARTER_LENGTH			1
#define AT07_THERMAL_CARTO_LENGTH			1
#define AT07_THERMAL_BOARD_LENGTH			1
#define AT07_RUN_INFO_LENGTH			8
#define AT07_LIGHTS_LENGTH			1
#define AT07_LIGHTS_LENGTH			1
#define AT07_BLINKERS_LENGTH			1
#define AT07_BLINKERS_LENGTH			1
#define AT07_FL_LIGHT_LENGTH			2
#define AT07_FL_LIGHT_LENGTH			2
#define AT07_FR_LIGHT_LENGTH			2
#define AT07_FR_LIGHT_LENGTH			2
#define AT07_BL_LIGHT_LENGTH			2
#define AT07_BL_LIGHT_LENGTH			2
#define AT07_BR_LIGHT_LENGTH			2
#define AT07_BR_LIGHT_LENGTH			2
#define AT07_BLINK_TIME_LENGTH			2
#define AT07_BLINK_TIME_LENGTH			2
#define AT07_LIGHTS_STATUS_LENGTH			0
#define AT07_MSG_PILOT1_LENGTH			8
#define AT07_MSG_PILOT2_LENGTH			8
#define AT07_MSG_PILOT3_LENGTH			8
#define AT07_SPEED_LENGTH			4
#define AT07_BATTERY_LENGTH			1
#define AT07_POSITION_LAT_LENGTH			4
#define AT07_POSITION_LONG_LENGTH			4
#define AT07_POSITION_MODE_LENGTH			1
#define AT07_ELEC_MOTOR_FORWARD_LENGTH			1
#define AT07_ELEC_MOTOR_BACKWARD_LENGTH			1
#define SEND_CAN_MSG			 bHIL_CAN_Send
        
        
        
typedef union {

    struct {
        uint8_t idType;
        uint32_t id;
        uint8_t dlc;
        uint8_t data0;
        uint8_t data1;
        uint8_t data2;
        uint8_t data3;
        uint8_t data4;
        uint8_t data5;
        uint8_t data6;
        uint8_t data7;
    } frame;
    uint8_t array[14];
} uCAN_MSG;

/**
 Defines
*/

#define dSTANDARD_CAN_MSG_ID_2_0B 1
#define dEXTENDED_CAN_MSG_ID_2_0B 2

/**
  Section: ECAN APIs
*/

/**
    @Summary
        Initializes the ECAN module. 

    @Description
        This routine sets all the set parameters to the ECAN module.

    @Preconditions 
        None

    @Param
        None
  
    @Returns
        None
 
    @Example
        <code>
        ECAN_Initialize();   
        </code>                                                                      
*/
void ECAN_Initialize(void);

/**
                                                                           
    @Summary
        CAN_sleep

    @Description
        Puts the CAN module to sleep

    @Param
        None 

    @Returns
        None   

    @Example
        <code>
        CAN_init();  
        </code> 
                                                                           
*/

void CAN_sleep(void);

/**
    @Summary
        CAN_transmit

    @Description
        Transmits out sCAN_MSG

    @Param
        Pointer to a sCAN_MSG

    @Returns
        True or False if message was loaded to transmit buffer 

    @Example
        <code>
        uCAN_MSG txMessage;
        CAN_transmit(&txMessage);  
        </code>                                                                             
*/
uint8_t CAN_transmit(uCAN_MSG *tempCanMsg);


/**

	@Summary
		CAN_receive

	@Description
		Receives CAN messages

	@Param
		Pointer to a sCAN_MSG

	@Returns
		True or False for a new message 
  
	@Example
		<code>
		uCAN_MSG rxMessage;
		CAN_receive(&rxMessage);  
		</code> 
                                                                             
*/
uint8_t CAN_receive(uCAN_MSG *tempCanMsg);

/**
 
	@Summary
		CAN_messagesInBuffer
 
	@Description
		Checks to see how many messages are in the buffer
	
	@Param
		None

	@Returns
		Returns total number of messages in the buffers

	@Example
		<code>
		uint8_t nrMsg;
		nrMsg = CAN_messagesInBuffer();  
		</code>                                                                            
*/
uint8_t CAN_messagesInBuffer(void);

/**

	@Summary
		CAN_isBusOff

	@Description
		Checks to see if module is busoff

	@Param
		None

	@Returns
		True if module is in Busoff, False is if it is not

	@Example
		<code>
		uint8_t busOff;
		busOff = CAN_isBusOff();  
		</code> 
                                                  
*/

uint8_t CAN_isBusOff(void);

/**

	@Summary
		CAN_isRXErrorPassive

	@Description
		Checks to see if module is RX Error Passive
        
	@Param
		None
 
	@Returns
 		True if module is in RX Error Passive, False is if it is not                                                    

 	@Example
		<code>
		uint8_t errRxPasive;
		errRxPasive = CAN_isRXErrorPassive();
		</code>     
                                                    
 */
 
uint8_t CAN_isRXErrorPassive(void);

/**

	@Summary
		CAN_isTXErrorPassive

	@Description
		Checks to see if module is TX Error Passive

	@Param
		None

	@Returns
		True if module is in TX Error Passive, False is if it is not

	@Example
		<code>
		uint8_t errTxPasive;
		errTxPasive = CAN_isTXErrorPassive();  
		</code>       

*/

uint8_t CAN_isTXErrorPassive(void);


#ifdef __cplusplus  // Provide C++ Compatibility
    }
#endif

#endif // ECAN_H

/**
 End of File
*/
