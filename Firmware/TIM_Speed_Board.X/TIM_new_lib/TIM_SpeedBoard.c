/*
 * File:   TIM_SpeedBoard.c
 * Author: Sofiane AOUCI
 *
 * Created on March 16, 2019, 1:59 PM
 */

#include "TIM_SpeedBoard.h"
#include <xc.h>



/*------------------ Private Variables -------------------*/
uint8_t start;
/*------------------ Private Prototypes ------------------*/
void TIM_SpeedBoard_ClockInit();
void TIM_SpeedBoard_GPIOInit();
void TIM_SpeedBoard_TimerInit();
void TIM_SpeedBoard_InterruptsInit();
/*------------------ Public Functions --------------------*/
void TIM_SpeedBoard_Init()
{
    int index = 0;
    TIM_SpeedBoard_ClockInit();
    TIM_SpeedBoard_GPIOInit();
    TIM_SpeedBoard_TimerInit();
    TIM_SpeedBoard_InterruptsInit();
    ECAN_Initialize();
    magnet = 0;
    for(index = 0; index < NB_MAGNETS; index++) intervals[index] = 0;
    start = 1;
    TIM_Speed = TIM_SpeedBoard_GetSpeed;
    TIM_TimeOut = 0;
    
    TIM_GPIO_WriteBits(TIM_LEDS_PORT, TIM_LED_STATUS_PIN, HIGH);
}

void TIM_SpeedBoard_SendSpeed(uint32_t speed)
{
    speed *= 1;
    
    uCAN_MSG tempCanMsg;

    tempCanMsg.frame.idType = dEXTENDED_CAN_MSG_ID_2_0B;
    tempCanMsg.frame.id = ((uint32_t)AT07_SPEED_DATA << 16) | (DASHBOARD_ADDR << 8) | (POSITION_MONITORING_ADDR);
    tempCanMsg.frame.dlc = 4;
    tempCanMsg.frame.data0 = (speed & 0xFF000000) >> 24;
    tempCanMsg.frame.data1 = (speed & 0xFF0000) >> 16;
    tempCanMsg.frame.data2 = (speed & 0xFF00) >> 8;
    tempCanMsg.frame.data3 = (speed & 0xFF);
    
    CAN_transmit(&tempCanMsg);
}

uint32_t TIM_SpeedBoard_GetSpeed()
{
    long float Speed = 0;
    int i = 0;
    for(i = 0; i < NB_MAGNETS; i++)
    {
        Speed += (long double)((WHEEL_CIRCUMFERNCE_MM * 3600 * (TIM0_Freq/1000000.0/TIM0_Prescaler)) / (intervals[i] * TIM0_CounterSize * NB_MAGNETS)); //(km/h)
    }
    Speed /= NB_MAGNETS;
    
    return (uint32_t) (Speed * Speed_Precision); //(km/h)
}

/*------------------ Private Functions -------------------*/
void TIM_SpeedBoard_ClockInit()
{
    TIM_INTOSC_InitTypDef INTOSC_InitDef;
    
    INTOSC_InitDef = new_INTOSC_InitDef();
    INTOSC_InitDef.freq = HF_64MHz;
    
    TIM_Clock_Init((TIM_CLOCK_InitTypDef*) &INTOSC_InitDef);
}

void TIM_SpeedBoard_GPIOInit()
{
    TIM_GPIO_InitTypDef GPIO_InitDef;
    
    GPIO_InitDef = new_GPIO_InitDef();
    
    // Set pins RB(0-2) in input mode for external interrupts (sensor)
    GPIO_InitDef.mode = GPIO_Mode_IN;
    GPIO_InitDef.port = TIM_SENSORS_PORT;
    GPIO_InitDef.pin  =  TIM_TOP_SENSOR_PIN | TIM_MIDDLE_SENSOR_PIN | TIM_BOTTOM_SENSOR_PIN;
    
    TIM_GPIO_Init(&GPIO_InitDef);
    
    // Set RA(0-3) in output mode for status LEDs
    GPIO_InitDef.mode = GPIO_Mode_OUT;
    GPIO_InitDef.port = TIM_LEDS_PORT;
    GPIO_InitDef.pin =  TIM_LED_STATUS_PIN | TIM_LED_TOP_SENSOR_PIN | TIM_LED_MIDDLE_SENSOR_PIN | TIM_LED_BOTTOM_SENSOR_PIN;
    
    TIM_GPIO_Init(&GPIO_InitDef);
    TIM_GPIO_WriteBits(GPIO_InitDef.port, GPIO_InitDef.pin, LOW);
    
    // SET RB3 to control CAN_STBY PIN
        GPIO_InitDef.port = TIM_CAN_STDBY_PORT;
    GPIO_InitDef.pin =  TIM_CAN_STDBY_PIN;
    
    TIM_GPIO_Init(&GPIO_InitDef);
    TIM_GPIO_WriteBits(GPIO_InitDef.port, GPIO_InitDef.pin, HIGH);
    
    /*TRISAbits.TRISA0 = 0;
    TRISAbits.TRISA1 = 0;
    TRISAbits.TRISA2 = 0;
    TRISAbits.TRISA3 = 0;
    
    TRISBbits.TRISB0 = 1;
    TRISBbits.TRISB1 = 1;
    TRISBbits.TRISB2 = 1;
    
    
    TRISBbits.TRISB3 = 0;
    ANCON1 = 0;
    
    
    LATAbits.LA0 = 0;LATAbits.LA3 = 0;LATAbits.LA2 = 0;LATAbits.LA1 = 0;LATBbits.LATB3 = 1;*/
}

void TIM_SpeedBoard_InterruptsInit()
{
    TIM_TIMER0_INTERRUPT->TIMER0_ENABLE = 1; //TIMER0 interrupt enable bit 
    TIM_TIMER0_INTERRUPT->TIMER0_FLAG = 0;   //clearing TIMER0 flag 
    
    TIM_EXTI->INT0_FLAG = 0 ; //INT0 flag
    TIM_EXTI->INT1_FLAG = 0 ; //INT1 flag
    TIM_EXTI->INT2_FLAG = 0 ; //INT2 flag
    
#ifdef TIM_BOTTOM_SENSOR
    TIM_EXTI->INT0_ENABLE = 1 ; //INT0 enable bit
    TIM_EXTI->INT0_ON_RISING = 0 ; // INT0 interrupt on rising edge 1 / falling 0
#endif
    
#ifdef TIM_TOP_SENSOR
    TIM_EXTI->INT1_ENABLE = 1 ; //INT1 enable bit
    TIM_EXTI->INT1_ON_RISING = 0 ; // INT1 interrupt on rising edge 1 / falling 0
#endif
    
#ifdef TIM_MIDDLE_SENSOR
    TIM_EXTI->INT2_ENABLE = 1 ; //INT2 enable bit
    TIM_EXTI->INT2_ON_RISING = 0 ; // INT2 interrupt on rising edge 1 / falling 0
#endif
    
    TIM_EXTI->GLOBAL_INT_ENABLE = 1 ; //Global interrupt enable
    //INTCONbits.PEIE=1;
    

}

void TIM_SpeedBoard_TimerInit()
{
    TIM0_Counter = 0;
    
    TIM_TIMER0->COUNTER_SIZE = T0_8BIT;
    TIM_TIMER0->CLOCK_SOURCE = CLKO; // Internal instruction cycle clock
    TIM_TIMER0->PRESCALER_DISABLE = 0;
    TIM_TIMER0->PRESCALER_VALUE = PV_8;
    TIM_TIMER0_SET_COUNTER_VALUE(0);
    TIM_TIMER0->ON = 1;
    
    /*T0CONbits.T08BIT = 1 ; // 8 bit counter enabled
    T0CONbits.T0CS = 0 ; //clock source
    T0CONbits.PSA = 0 ; //prescaler enabled
    T0CONbits.T0PS2 = 0 ;
    T0CONbits.T0PS1 = 1 ;
    T0CONbits.T0PS0 = 0 ;
    TMR0 = 0 ;
    //8 bit counter, with prescaler = 8
    T0CONbits.TMR0ON = 1 ; //TIMER0 enabled*/
    
}

void interrupt tc_int(void)
{
	if (TIM_TIMER0_INTERRUPT->TIMER0_FLAG == 1)
    {  
            TIM_TIMER0_INTERRUPT->TIMER0_FLAG = 0;				//clear interrupt flag
            ++TIM0_Counter;
    }
            
    if(TIM_EXTI->INT0_FLAG == 1)
    {
        if(!start)
        {
            passage[1] = TIM0_Counter;
            intervals[magnet] = passage[1] - passage[0];
            passage[0] =  passage[1];
            magnet = ++magnet % (NB_MAGNETS);
        }
        else
        {
            passage[0] = TIM0_Counter;
            start = 0;
        }
        TIM_GPIO_ToggleBits(TIM_LEDS_PORT, TIM_LED_BOTTOM_SENSOR_PIN);
        TIM_EXTI->INT0_FLAG = 0;
    }
    
    if(TIM_EXTI->INT1_FLAG == 1)
    {
        if(!start)
        {
            passage[1] = TIM0_Counter;
            intervals[magnet] = passage[1] - passage[0];
            passage[0] =  passage[1];
            magnet = ++magnet % (NB_MAGNETS);
        }
        else
        {
            passage[0] = TIM0_Counter;
            start = 0;
        }
        TIM_GPIO_ToggleBits(TIM_LEDS_PORT, TIM_LED_BOTTOM_SENSOR_PIN);
        TIM_EXTI->INT1_FLAG = 0;
    }
    
    if(TIM_EXTI->INT2_FLAG == 1)
    {
        if(!start)
        {
            passage[1] = TIM0_Counter;
            intervals[magnet] = passage[1] - passage[0];
            passage[0] =  passage[1];
            magnet = ++magnet % (NB_MAGNETS);
        }
        else
        {
            passage[0] = TIM0_Counter;
            start = 0;
        }
        TIM_GPIO_ToggleBits(TIM_LEDS_PORT, TIM_LED_MIDDLE_SENSOR_PIN);
        TIM_EXTI->INT2_FLAG = 0;
    }
}
    
    