/********************************************************************
* file TIM_CLOCK.h
* brief CLOCK configuration Lib
* author Sofiane AOUCI for TIM
* version 1.0
* date 24/02/2019 
********************************************************************/

#ifndef TIM_CLOCK_H
#define	TIM_CLOCK_H
#include <xc.h>
#include <stdint.h>
#include <stdbool.h>

typedef enum
{
	 HF_64MHz  = 0b101111,
	 HF_32MHz  = 0b101110,
	 HF_16MHz  = 0b100111,
	 HF_8MHz   = 0b100110,
	 HF_4MHz   = 0b100101,
	 HF_2MHz   = 0b100100,
	 HF_1MHz   = 0b100011,
	 HF_500KHz = 0b100010,
	 HF_250KHz = 0b100001,
	 HF_31KHz  = 0b100000,
	 MF_500KHz = 0b110010,
	 MF_250KHz = 0b110001,
	 MF_31KHz  = 0b110000,
	 LF_31KHz  = 0b000000
} TIM_INTOSCfreq; // INTERNAL CLOCK FREQUENCY

typedef enum
{
	SLEEP = 0  ,
    IDLE  = 1
} TIM_IDLEN; // POWER MANAGER MODE (SLEEP, IDLEN)

typedef enum
{
	INTOSC 	,
	OSC 	,
	SOSC
} TIM_CLOCK; // CLOCK TYPE (INTERNAL, EXTERNAL, ... etc)

typedef struct
{
	TIM_CLOCK clock;
	TIM_INTOSCfreq freq;
    TIM_IDLEN idlen;
} TIM_INTOSC_InitTypDef; // INTERNAL CLOCK INIT STRUCT

/*--------------------------------------------- ABSTRACT STRUCT ------------------------------------------*/
typedef struct
{
    TIM_CLOCK clock;
    TIM_IDLEN idlen;
} TIM_CLOCK_InitTypDef; // CLOCK INIT STRUCT (used only as cast for the pointer in TIM_initClock() function)
/**********************************************************************************************************/

TIM_INTOSC_InitTypDef new_INTOSC_InitDef();
void TIM_Clock_Init(TIM_CLOCK_InitTypDef* CLOCK_InitDef);
uint32_t Clock_Get_Frequency();
void Clock_Delay(uint32_t delayms);

#endif