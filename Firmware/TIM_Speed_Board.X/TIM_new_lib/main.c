/********************************************************************
* file: 	main.c
* brief: 	main program
* author: 	Sofiane AOUCI - Prince JACQUET
* version: 	1.1
* date:		07/2019 
********************************************************************/

#include "TIM_SpeedBoard.h"

uint32_t t_speed = 0;
uint32_t t_last_speed = 0;

void main(void) {
    TIM_SpeedBoard_Init();
    while(1)
    {
        //if there is no passage for 2s, the speed is null, otherwise, we calculate it using TIM_Speed(); 
        if(TIM0_Counter - passage[0] >  (int)((TIM_TIMEOUT * TIM0_Freq) / (TIM0_CounterSize * TIM0_Prescaler)))  {t_speed = 0;}
        else t_speed = TIM_Speed();
        
        //We send data over the CAN whenever the speed changes
        if(t_last_speed != t_speed)
        {
            TIM_GPIO_WriteBits(TIM_CAN_STDBY_PORT,TIM_CAN_STDBY_PIN,0);
        
            if(!CAN_isBusOff()) TIM_SpeedBoard_SendSpeed(t_speed);
            
            Clock_Delay(50);
            
            TIM_GPIO_WriteBits(TIM_CAN_STDBY_PORT,TIM_CAN_STDBY_PIN,1);
            
            t_last_speed = t_speed;
        }
        Sleep(); //We enter IDLE mode
    }
    return;
}


