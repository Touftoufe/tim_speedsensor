#ifndef TIM_SPEEDBOARD_CONTROL_H
#define TIM_SPEEDBOARD_CONTROL_H

/* 
 * File:   
 * Author: 
 * Comments:
 * Revision history: 
 */



/*--------------------------------------- CAR Selection ----------------------------------------*/
#define TIM_TIM7_TEST_Banc
//#define TIM_TIM8_TEST_Banc
//#define TIM_TIM7
//#define TIM_TIM8

/*-------------------------------------- SENSOR Selection --------------------------------------*/
//#define TIM_TOP_SENSOR
#define TIM_MIDDLE_SENSOR
//#define TIM_BOTTOM_SENSOR

/*-------------------------------------- USER GPIO PINS ----------------------------------------*/

#define TIM_LEDS_PORT               GPIOA
#define TIM_LED_STATUS_PIN          GPIO_Pin_0
#define TIM_LED_TOP_SENSOR_PIN      GPIO_Pin_1
#define TIM_LED_MIDDLE_SENSOR_PIN   GPIO_Pin_2
#define TIM_LED_BOTTOM_SENSOR_PIN   GPIO_Pin_3

#define TIM_CAN_STDBY_PORT          GPIOB
#define TIM_CAN_STDBY_PIN           GPIO_Pin_3

#define TIM_SENSORS_PORT            GPIOB
#define TIM_TOP_SENSOR_PIN          GPIO_Pin_1
#define TIM_MIDDLE_SENSOR_PIN       GPIO_Pin_2
#define TIM_BOTTOM_SENSOR_PIN       GPIO_Pin_0



#endif