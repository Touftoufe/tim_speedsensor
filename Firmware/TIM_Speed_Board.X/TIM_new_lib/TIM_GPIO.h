/********************************************************************
* file: 	TIM_GPIO.h
* brief: 	GPIO configuration Lib
* author: 	Sofiane AOUCI for TIM
* version: 	1.0
* date:		24/02/2019 
********************************************************************/

#ifndef TIM_GPIO_H
#define TIM_GPIO_H
#include <xc.h>
#include <stdint.h>
#include <stdbool.h>

typedef enum
{
	HIGH = 1,
	LOW = 0
}TIM_GPIO_State;
typedef enum
{
	GPIOA = (uint8_t) &PORTA,
	GPIOB = (uint8_t) &PORTB,
	GPIOC = (uint8_t) &PORTC	
} TIM_GPIO_Port;

typedef enum
{
	GPIO_Pin_0   = (0x01 << (0x10 + 2)) | (0x01 << (0x08 + 0)) | 0x01 << 0,
	GPIO_Pin_1   = (0x01 << (0x10 + 0)) | (0x01 << (0x08 + 1)) | 0x01 << 1,
	GPIO_Pin_2   =                        (0x01 << (0x08 + 2)) | 0x01 << 2,
	GPIO_Pin_3   =                        (0x01 << (0x08 + 3)) | 0x01 << 3,
	GPIO_Pin_4   = (0x01 << (0x10 + 1)) |                        0x01 << 4,
	GPIO_Pin_5   =                        (0x01 << (0x08 + 4)) | 0x01 << 5,
	GPIO_Pin_6   = 0x01 << 6,
	GPIO_Pin_7   = 0x01 << 7,

} TIM_GPIO_Pin;

typedef enum
{
	GPIO_Mode_OUT = 0,
	GPIO_Mode_IN = 1,
	GPIO_Mode_AN,	
	GPIO_Mode_AF
}TIM_GPIO_Mode;

typedef struct
{
	TIM_GPIO_Pin pin;
	TIM_GPIO_Port port;
	TIM_GPIO_Mode mode;
	bool pull_up;
} TIM_GPIO_InitTypDef;

TIM_GPIO_InitTypDef  new_GPIO_InitDef();
void TIM_GPIO_Init(TIM_GPIO_InitTypDef* GPIO_InitDef);
void TIM_GPIO_WriteBits(TIM_GPIO_Port GPIOx, TIM_GPIO_Pin pins, TIM_GPIO_State state);
TIM_GPIO_State TIM_GPIO_ReadBit(TIM_GPIO_Port GPIOx, TIM_GPIO_Pin pin);
void TIM_GPIO_ToggleBits(TIM_GPIO_Port GPIOx, TIM_GPIO_Pin pins);
#endif