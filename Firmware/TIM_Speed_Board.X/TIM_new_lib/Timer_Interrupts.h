#ifndef TIMER_INTERRUPTS_H
#define TIMER_INTERRUPTS_H

#include <stdint.h>
uint32_t volatile TIM0_Counter;

void Interr_Init();
void SpeedTimerInit();

#endif