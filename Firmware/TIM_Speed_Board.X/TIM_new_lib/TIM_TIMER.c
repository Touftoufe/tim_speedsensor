#include "TIM_TIMER.h"

void TIM_TIMER0_SET_COUNTER_VALUE(uint16_t value)
{
    if(TIM_TIMER0->COUNTER_SIZE == T0_16BIT) TMR0H = (value >> 8) & 0xFF;
    TMR0L = value & 0xFF;
}
