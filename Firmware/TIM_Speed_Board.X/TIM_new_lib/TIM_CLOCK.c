#include "TIM_CLOCK.h"

int pow(int x, int n);
TIM_INTOSC_InitTypDef* INTOSC_InitDef_c;

uint32_t CLOCK_FREQ = 0;

void TIM_Clock_Init(TIM_CLOCK_InitTypDef* CLOCK_InitDef)
{
	switch(CLOCK_InitDef->clock)
	{
		case INTOSC:
			INTOSC_InitDef_c = (TIM_INTOSC_InitTypDef*) CLOCK_InitDef;

		    OSCCONbits.IRCF 	= INTOSC_InitDef_c->freq & 0b000111;
			OSCTUNEbits.PLLEN   = (INTOSC_InitDef_c->freq & 0b001000) >> 3;
		    OSCCON2bits.MFIOSEL = (INTOSC_InitDef_c->freq & 0b010000) >> 4;
		    OSCTUNEbits.INTSRC  = (INTOSC_InitDef_c->freq & 0b100000) >> 5;
		    OSCCONbits.SCS 		= 0;
            OSCCONbits.IDLEN = INTOSC_InitDef_c->idlen;
		    CLOCK_FREQ = (!OSCCONbits.IRCF * 31250) + (OSCCONbits.IRCF > 0) * (pow(2,OSCCONbits.IRCF-1)*250000) * pow(4,OSCTUNEbits.PLLEN); 
		    while(!(!OSCTUNEbits.INTSRC || (OSCTUNEbits.INTSRC && (OSCCON2bits.MFIOSEL || OSCCONbits.HFIOFS) && OSCCONbits.OSTS) ));
		break; 
	}
}

uint32_t Clock_Get_Frequency()
{
	return CLOCK_FREQ;
}

TIM_INTOSC_InitTypDef new_INTOSC_InitDef()
{
	TIM_INTOSC_InitTypDef INTOSC_InitDef;
	INTOSC_InitDef.clock = INTOSC;
	INTOSC_InitDef.freq  = HF_8MHz;
    INTOSC_InitDef.idlen = IDLE;
	return INTOSC_InitDef;
}

void Clock_Delay(uint32_t delayms)
{
    uint32_t __u32_i;
    uint32_t __delay;
    __delay=(uint32_t)((float)CLOCK_FREQ/64000000.0*(0x7A120*1.75*delayms)/1000.0);
    for(__u32_i=0;__u32_i<__delay;__u32_i++);
}

int pow(int x, int n)
{
    int y = 1;
	while(n--) y *= x;
    return y;
}