/* 
 * File:   TIM_TIMER.h
 * Author: beusaile
 *
 * Created on July 19, 2019, 12:19 PM
 */

#ifndef TIM_TIMER_H
#define	TIM_TIMER_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <xc.h>
#include <stdint.h>
#include <stdbool.h>
    
    
typedef enum {
        PV_2   = (unsigned)0,
        PV_4   = (unsigned)1,
        PV_8   = (unsigned)2,
        PV_16  = (unsigned)3,
        PV_32  = (unsigned)4,
        PV_64  = (unsigned)5,
        PV_128 = (unsigned)6,
        PV_256 = (unsigned)7
} PRESCALER_VALUE_t; 

typedef enum {
        INCREMENT_ON_HIGH_TO_LOW  = (unsigned)1,
        INCREMENT_ON_LOW_TO_HIGH  = (unsigned)0,
        IOHL                      = (unsigned)1,
        IOLH                      = (unsigned)0
} SOURCE_EDGE_t; 
 
typedef enum {
        T0CKI_PIN = (unsigned)1,
        CLKO      = (unsigned)0   // Internal instruction cycle clock
} CLOCK_SOURCE_t; 

typedef enum {
        T0_8BIT   = (unsigned)1,
        T0_16BIT  = (unsigned)0
} COUNTER_SIZE_t;

typedef struct {
        unsigned PRESCALER_VALUE          :3;
        unsigned PRESCALER_DISABLE        :1;
        unsigned SOURCE_EDGE              :1;
        unsigned CLOCK_SOURCE             :1;
        unsigned COUNTER_SIZE             :1;
        unsigned ON                       :1;
} TIM_TMR0_t;

typedef struct {
        unsigned                        :2;
        unsigned TIMER0_FLAG            :1;
        unsigned                        :2;
        unsigned TIMER0_ENABLE          :1;
        unsigned                        :1;
        unsigned GLOBAL_INT_ENABLE      :1;
} TIM_TMR0_INT_t;

TIM_TMR0_t *TIM_TIMER0 = (TIM_TMR0_t*) &T0CONbits;
TIM_TMR0_INT_t *TIM_TIMER0_INTERRUPT = (TIM_TMR0_INT_t*) &INTCONbits;

void TIM_TIMER0_SET_COUNTER_VALUE(uint16_t value);



#ifdef	__cplusplus
}
#endif

#endif	/* TIM_TIMER_H */

